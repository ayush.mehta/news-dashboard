package com.example.news.dao;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.example.news.model.News;

import com.example.news.repository.NewsRepo;

@Component
public class JsonParse {
	String url;


	@Autowired
	NewsRepo nR;
	List<News> list = new ArrayList<News>();

	public void saveJson(String country, String category) {
		String sourceName = null;
		String title = null;
		String description = null;
		String imageUrl = null;
		String published = null;
		String content = null;

		if (country != null && category != null) {
			url = "https://newsapi.org/v2/top-headlines?country=" + country + "&category=" + category
					+ "&apiKey=544258eff12d4a538105d809bcd28b8e";
		} else if (country != null && category == null) {
			url = "https://newsapi.org/v2/top-headlines?country=" + country
					+ "&apiKey=544258eff12d4a538105d809bcd28b8e";
		} else if (country == null && category != null) {
			url = "https://newsapi.org/v2/top-headlines?category=" + category
					+ "&apiKey=544258eff12d4a538105d809bcd28b8e";
		}
		RestTemplate restTemplate = new RestTemplate();
		String resp = restTemplate.getForObject(url, String.class);
		JSONObject root = new JSONObject(resp);
		JSONArray articles = root.getJSONArray("articles");

		for (int i = 0; i < articles.length(); i++) {
			JSONObject obj = articles.getJSONObject(i);
			JSONObject source = obj.getJSONObject("source");

			News na = new News();

			if (!org.json.JSONObject.NULL.equals(source.opt("name")) && source.getString("name") != null) {
				sourceName = (String) source.get("name");
			}
			if (!org.json.JSONObject.NULL.equals(obj.opt("title")) && obj.getString("title") != null) {
				title = (String) obj.get("title");
			}
			if (!org.json.JSONObject.NULL.equals(obj.opt("description")) && obj.getString("description") != null) {
				description = (String) obj.get("description");
			}
			if (!org.json.JSONObject.NULL.equals(obj.opt("urlToImage")) && obj.getString("urlToImage") != null) {
				imageUrl = (String) obj.get("urlToImage");
			}
			if (!org.json.JSONObject.NULL.equals(obj.opt("publishedAt")) && obj.getString("publishedAt") != null) {
				published = (String) obj.get("publishedAt");
			}
			if (!org.json.JSONObject.NULL.equals(obj.opt("content")) && obj.getString("content") != null) {

				content = (String) obj.get("content");
			}
			na.setSourceName(sourceName);
			na.setTitle(title);
			na.setDescription(description);
			na.setImageUrl(imageUrl);
			na.setPublished(published);
			na.setContent(content);
			na.setFilter(1);
			list.add(na);

		}
		nR.saveAll(list);
	}
}
