package com.example.news.repository;
import org.springframework.data.repository.CrudRepository;

import com.example.news.model.News;

public interface NewsRepo extends CrudRepository<News, Integer> {

}
