package com.example.news.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class News {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int sr;
	private String source_name;
	private String title;
	private String description;
	private String image_url;
	private String published;
	private String content;
	private int filter_master_f_id;

	public String getSourceName() {
		return source_name;
	}

	public void setSourceName(String sourceName) {
		this.source_name = sourceName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return image_url;
	}

	public void setImageUrl(String imageUrl) {
		this.image_url = imageUrl;
	}

	public String getPublished() {
		return published;
	}

	public void setPublished(String published) {
		this.published = published;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getFilter() {
		return filter_master_f_id;
	}

	public void setFilter(int filter) {
		filter_master_f_id = filter;
	}

}
